/*
** EPITECH PROJECT, 2020
** qt-engine
** File description:
** ECheckBox
*/

#pragma once

#include "Object.hpp"
#include <QtWidgets/QCheckBox>

namespace libraryObjects {
	typedef Object<QCheckBox> ECheckBox;
}
