/*
** EPITECH PROJECT, 2020
** qt-engine
** File description:
** EAbstractButton
*/

#pragma once

#include "Object.hpp"
#include <QtWidgets/QAbstractButton>

namespace libraryObjects {
	typedef Object<QAbstractButton> EAbstractButton;
}
